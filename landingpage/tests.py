from django.test import TestCase, Client
from django .http import HttpRequest
from .views import add_form

# Create your tests here.
class AppUnitTest(TestCase):

    #Test untuk cek alamat url telah ada
    def test_url_address_exist(self):
        response = Client().get('/landingpage/add_form/')
        self.assertEqual(response.status_code, 200)

##    #Test untuk cek alamat url tidak ada
##    def test_url_address_doesnt_exist(self):
##        response = Client().get('/landingpage/')
##        self.assertNotEqual(response.status_code, 200)
##
##    #Test untuk cek fungsi add_form telah memiliki isi
##    def test_add_form_is_written(self):
##        self.assertIsNotNone(add_form)
##
##    #Test untuk cek apakah template html telah digunakan atau belum
##    def test_template_html_is_exist(self):
##        response = Client().get('/landingpage/add_form/')
##        self.assertTemplateUsed(response, 'add_form.html')
##
##    #Test untuk cek Halo, apa kabar? sudah ada di dalam landing page
##    #def test_string_ucapan_is_not_none(self):
##        #request = HttpRequest()
##        #response = add_form(request)
##        #html_response = response.content.decode('utf8')
##        #self.assertIn("Halo, apa kabar?", html_response)
##
##    def test_content_string_in_html_is_exist(self):
##        response = Client().get('/landingpage/add_form/')
##        self.assertContains(response, 'Hallo, apa kabar?')
